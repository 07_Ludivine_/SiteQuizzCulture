<?php
require'include/header.php';
require'include/nav_G.php';
require'include/alert.php';
logged_only();
admin_only();

if (isset($_POST["valider"])) {

    $idquestion = htmlspecialchars($_POST['idquestion']);
    $reqvalid = $pdo->prepare('UPDATE question SET verifie=1 WHERE idquestion=?');
    $reqvalid->execute([$idquestion]);
}
if (isset($_POST["supprimer"])) {

    $idquestion = htmlspecialchars($_POST['idquestion']);
    $reqsuppr = $pdo->prepare('DELETE FROM question WHERE idquestion=?');
    $reqsuppr->execute([$idquestion]);
}
?>

<?php
$reqn = $pdo->prepare('SELECT * FROM question WHERE verifie=99');
$reqn->execute();

while ($data = $reqn->fetch()) {
    ?>

    <label for="<?php $data->idquestion; ?>" class="col-sm-12">Question n°<?php echo $data->idquestion; ?>:</label>
    <div class="form-group col-sm-12">
        <p>Enoncé<?php echo $data->enonce_question; ?></p>
        <p class="col-sm-6 btn_primary btn">Choix 1:<?php echo $data->choix_rep1; ?></p>
        <p class="col-sm-6 btn_primary btn">Choix 2:<?php echo $data->choix_rep2; ?></p>
        <p class="col-sm-6 btn_primary btn">Choix 3:<?php echo $data->choix_rep3; ?></p>
        <p class="col-sm-6 btn_primary btn">Choix 4:<?php echo $data->choix_rep4; ?></p>
        <p>Réponse:<?php echo $data->reponse; ?></p>
    </div>
    <form action="" method="post">
        <input type="hidden" value="<?php echo $data->idquestion; ?>" name="idquestion"/>
        <button class="col-sm-12" type="submit" name="valider">Valider</button>
    </form>
    <form action="" method="post">  
        <input type="hidden" value="<?php echo $data->idquestion; ?>" name="idquestion"/>
        <button class="col-sm-12" type="submit" name="supprimer">Supprimer</button>
    </form> 

<?php } ?>
</br>
<a href="profil.php"><button title="Retour à la page profil" class="btn btn_primary  col-sm-12">Retour</button></a>
</br>
<?php
require"include/footer.php";
