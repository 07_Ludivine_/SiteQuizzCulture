<?php
require 'include/config.php';
require'include/functions.php';
logged_only();
require 'include/header.php';
require_once 'include/nav_G.php';

$act_pseudo = $_SESSION['auth']->pseudo;
$act_prenom = $_SESSION['auth']->prenom;
$act_nom = $_SESSION['auth']->nom;
$act_email = $_SESSION['auth']->email;
$id = $_SESSION['auth']->idmembre;

if (isset($_POST['pseudo']) AND $_POST['pseudo'] != $act_pseudo) {
    //on vérifie que la valeur postée correspond à la définition d'un pseudo
    if (empty($_POST['pseudo']) || !preg_match('/^[a-zA-Z0-9_]+$/', $_POST['pseudo'])) {
        $errors['pseudo'] = "Votre Pseudo n'est pas valide (aphanumérique)";
    } else {
    //si le nouveau pseudo choisi est valide on vérifie qu'il n'a pas déjà été pris
        $reqpseudo = $pdo->prepare('SELECT idmembre FROM membre WHERE pseudo = ?');
        $reqpseudo->execute([$_POST['pseudo']]);
        $pseudo = $reqpseudo->fetch();
        if ($pseudo) {
            $errors['pseudo'] = "Ce pseudo est déjà pris";
        } else {
        /*si le nouveau pseudo choisi est disponible on prepare et on execute la requete 
        pour modifier le pseudo de l'utilisateur dans la bdd*/
            $pdo->prepare('UPDATE membre SET pseudo = ? WHERE idmembre = ?')->execute([$_POST['pseudo'], $id]);
            $_SESSION['flash']['success'] = 'Votre pseudo a bien été modifié';
            $_SESSION['auth']->pseudo = $_POST['pseudo'];
            session_start();
            header('Location: majProfil.php');
            exit();
        }
    }
}

if (isset($_POST['prenom']) AND $_POST['prenom'] != $act_prenom) {
    if (empty($_POST['prenom']) || !preg_match("#^([a-zàáâäçèéêëìíîïñòóôöùúûü]+(( |')[a-z]+)*)+([-]([a-z]+(( |')[a-z]+)*)+)*$#iu", $_POST['prenom'])) {
        $errors['prenom'] = "Votre Prénom n'est pas valide (uniquement alphabétique)";
    } else {
        $pdo->prepare('UPDATE membre SET prenom = ? WHERE idmembre = ?')->execute([$_POST['prenom'], $id]);
        $_SESSION['flash']['success'] = 'Votre prenom a bien été modifié';
        $_SESSION['auth']->prenom = $_POST['prenom'];
        session_start();
        header('Location: majProfil.php');
        exit();
    }
}


if (isset($_POST['nom']) AND $_POST['nom'] != $act_nom) {
    if (empty($_POST['nom']) || !preg_match("#^([a-zàáâäçèéêëìíîïñòóôöùúûü]+(( |')[a-z]+)*)+([-]([a-z]+(( |')[a-z]+)*)+)*$#iu", $_POST['nom'])) {
        $errors['nom'] = "Votre Nom n'est pas valide (uniquement alphabétique)";
    } else {
        $pdo->prepare('UPDATE membre SET nom = ? WHERE idmembre = ?')->execute([$_POST['nom'], $id]);
        $_SESSION['flash']['success'] = 'Votre nom a bien été modifié';
        $_SESSION['auth']->nom = $_POST['nom'];
        session_start();
        header('Location: majProfil.php');
        exit();
    }
}

if (isset($_POST['email']) AND $_POST['email'] != $act_email) {
    if (empty($_POST['email']) || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
        $errors['email'] = "Votre Email n'est pas valide";
    } else {
        $reqemail = $pdo->prepare('SELECT idmembre FROM membre WHERE email = ?');
        $reqemail->execute([$_POST['email']]);
        $email = $reqemail->fetch();
        if ($email) {
            $errors['email'] = "Cet Email est déjà associé à un autre compte utilisateur";
        } else {
            if ($_POST['email2'] != $_POST['email']) {
                $errors['email2'] = "Vos Email ne sont pas identiques";
            } else {
//              $token = str_random(60);

                $req = $pdo->prepare('UPDATE membre SET email = ? WHERE idmembre = ?'/* , confirmation token */);
                $req->execute(/* [ */$_POST['email'], $id/* ,$token] */);
                /* $user_id = $pdo->lastInsertId(); 
                  mail($_POST['email'], 'Confirmation de votre mail', "Afin de confirmer votre mail merci de cliquer sur ce lien\n\nhttp://http://site/confirm.php?id=$user_id&token=$token");
                  $_SESSION['flash']['success']= 'Afin de valider votre changement de email, un email de confirmation vous a été envoyé, pour poursuivre votre navigation veuiller utiliser le lien que vous venez de recevoir'; */
                $_SESSION['flash']['success'] = 'Votre email a bien été modifié';
                $_SESSION['auth']->email = $_POST['email'];
                session_start();
                header('Location: majProfil.php');
                exit();
            }
        }
    }
}
?>
<h1>Modification Profil</h1>
<p>Changer, corriger mes informations personnelles</p>

<?php if (!empty($errors)): ?>
    <div class="alert alert-danger">
        <ul>
    <?php foreach ($errors as $error): ?>
                <li><?= $error; ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
        <?php endif; ?>

<form action="" method="POST">    
    <label for="pseudo" class="col-sm-12">Pseudo</label>
    <div class="form-group col-sm-9">
        <input class="form-control" type="text" name="pseudo" id="pseudo" placeholder="<?= $_SESSION['auth']->pseudo; ?>"/>
    </div>
    <button type="submit" class="btn btn_primary  col-sm-3 " name="new_pseudo">Changer de Pseudo</button>
</form>

<form action="" method="POST">
    <label for="prenom" class="col-sm-12">Prénom</label>    
    <div class="form-group col-sm-9">

        <input class="form-control" type="text" name="prenom" id="prenom" placeholder="<?= $_SESSION['auth']->prenom; ?>" />
    </div>
    <button type="submit" class="btn btn_primary col-sm-3" name="new_prenom">Changer le Prenom</button>
</form>

<form action="" method="POST">
    <label for="nom" class="col-sm-12" >Nom</label>
    <div class="form-group col-sm-9">

        <input class="form-control" type="text" name="nom" id="nom" placeholder="<?= $_SESSION['auth']->nom; ?>"/>
    </div>
    <button type="submit" class="btn btn_primary col-sm-3" name="new_nom">Changer le Nom</button>
</form>


<form action="" method="POST">
    <label for="email" class="col-sm-12">Email</label>
    <div class="form-group col-sm-9">   
        <input class="form-control" type="email" name="email" id="email" placeholder="<?= $_SESSION['auth']->email; ?>"/>

        <input class="form-control col-sm-9" type="email" name="email2" id="email2" placeholder="Confirmez votre Email"/>
    </div>
    <button type="submit" class="btn btn_primary col-sm-3" name="new_email">Changer de Email</button>

</form>

</br>
<a href="profil.php"><button title="Retour au profil" class="btn btn_primary  col-sm-12">Retour</button></a>
</br>
<?php require"include/footer.php"; ?>
