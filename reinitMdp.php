<?php
if (isset($_GET['id']) && isset($_GET['token'])) {
    require 'include/config.php';
    require'include/functions.php';
    $req = $pdo->prepare('SELECT * FROM membre WHERE idmembre = ? AND reset_token IS NOT NULL AND reset_token = ? AND reset_at > DATE_SUB(NOW(), INTERVAL 30 MINUTE)');
    $req->execute([$_GET['id'], $_GET['token']]);
    $user = $req->fetch();
    if ($user) {
        if (!empty($_POST)) {
            if (!empty($_POST['mdp']) && $_POST['mdp'] == $_POST['mdp2']) {
                $password = password_hash($_POST['mdp'], PASSWORD_BCRYPT);
                $pdo->prepare('UPDATE membre SET password = ?, reset_at = NULL, reset_token = NULL')->execute([$password]);
                session_start();
                $_SESSION['flash']['success'] = 'Votre mot de passe a bien été modifié';
                $_SESSION['auth'] = $user;
                header('Location: profil.php');
                exit();
            }
        }
    } else {
        session_start();
        $_SESSION['flash']['error'] = "Ce token n'est pas valide";
        header('Location: connexion.php');
        exit();
    }
} else {
    session_start();
    $_SESSION['flash']['danger'] = "Vous n'avez pas le droit d'acceder à cette page";
    header('Location: connexion.php');
    exit();
}
require 'include/header.php';
?>
<form action="" method="POST">

    <div class="form-group">
        <label for="mdp" >Nouveau Mot de passe</label>
        <input class="form-control" type="password" name="mdp" id="mdp" placeholder="Choisissez un Mot de passe"/>
    </div>
    <div class="form-group">
        <label for="mdp2" >Confirmation du Mot de passe</label>
        <input class="form-control" type="password" name="mdp2" id="mdp2" placeholder="Confirmez votre Mot de passe"/>
    </div>
    <button type="submit" class="btn btn_primary">Valider</button>

</form>
</br>
<a href="profil.php"><button title="Retour à la page profil" class="btn btn_primary  col-sm-12">Retour</button></a>
</br>
<?php
require"include/footer.php";
