<?php
require 'include/config.php';
require'include/functions.php';

if (!empty($_POST) && !empty($_POST['email'])) {

    $req = $pdo->prepare('SELECT * FROM membre WHERE email = ? AND confirmed_at IS NOT NULL');
    $req->execute([$_POST['email']]);
    $user = $req->fetch();
    if ($user) {
        session_start();
        $reset_token = str_random(60);
        $pdo->prepare('UPDATE membre SET reset_token = ?, reset_at = NOW() WHERE idmembre = ?')->execute([$reset_token, $user->id]);
        $_SESSION['flash']['success'] = 'Les instructions du rappel de mot de passe vous ont été envoyées par emails';
        mail($_POST['email'], 'Réinitialisation de votre mot de passe', "Afin de réinitialiser votre mot de passe merci de cliquer sur ce lien\n\nhttp://local.dev/Lab/Comptes/reinitMdp.php?id={$user->id}&token=$reset_token");
        header('Location: connexion.php');
        exit();
    } else {
        $_SESSION['flash']['danger'] = 'Aucun compte ne correspond à cet adresse';
    }
}
require 'include/header.php';
?>
<form method="POST" action="">
    <div class="form-group">
        <label for="username" >Votre Email</label>
        <input class="form-control" type="text" name="email" id="email" placeholder="Votre Email" value="<?php if (isset($_POST['email'])) {
    echo $_POST['email'];
} ?>"/>
    </div>
    <button type="submit" class="btn btn_primary col-sm-12" title="Recevoir un email de récupération de mot de passe">Confirmer</button>
</form>
</br>
<a href="connexion.php"><button title="Retour vers la page de connexion" class="btn btn_primary  col-sm-12">Retour</button></a>
</br>
<?php
require"include/footer.php";
