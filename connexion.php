<?php
require_once 'include/functions.php';
require_once 'include/config.php';

if (!empty($_POST) && !empty($_POST['username']) && !empty($_POST['mdp'])) {


    $req = $pdo->prepare('SELECT * FROM membre WHERE(pseudo = :username OR email = :username)'/* AND confirmed_at IS NOT NULL' */);
    $req->execute(['username' => $_POST['username']]);
    $user = $req->fetch();
    /* Vérifie l'existence de l'utilisateur dans la base de donnée */
    $req = $pdo->prepare('SELECT * FROM acces WHERE idacces =' . $user->acces_idacces . '');
    $req->execute();
    $acces = $req->fetch();
    $user->niveauAcces = $acces->niveauAcces;
    /* configure la session utilisateur */
    if ($user == null) {
        $_SESSION['flash']['danger'] = 'Identifiant ou mot de passe incorrecte';
    } elseif (password_verify($_POST['mdp'], $user->mdp)) {
        session_start();
        $_SESSION['auth'] = $user;
        $_SESSION['flash']['success'] = 'Vous êtes maintenant connecté';
        if ($_POST['remember']) {
            $remember_token = str_random(250);
            $pdo->prepare('UPDATE membre SET remember_token = ? WHERE idmembre = ? ')->execute([$remember_token, $user->idmembre]);
            setcookie('remember', $user->idmembre . '==' . $remember_token . sha1($user->idmembre, 'paquerette'), time() * 60 * 60 * 24 * 7);
            die();
        }

        header('Location: profil.php');
        exit();
    } else {
        $_SESSION['flash']['danger'] = 'Identifiant ou mot de passe incorrecte';
    }
}
?>
<?php
require_once'include/header.php';
require_once'include/nav_G_I.php';
?>

<h1>Connexion</h1>
<form method="POST" action="">
    <div class="form-group">
        <label for="username" >Votre Pseudo ou Email</label>
        <input class="form-control" type="text" name="username" id="username" placeholder="Pseudo ou Email" value="<?php
        if (isset($_POST['username'])) {
            echo $_POST['username'];
        }
        ?>"required/>
    </div>
    <div class="form-group">
        <label for="mdp" >Votre Mot de passe<a href="recupMdp.php"> mot de passe oublié</a></label>
        <input class="form-control" type="password" name="mdp" id="mdp" placeholder="Mot de passe" required/>
    </div>
    <div class="form-group">
        <label>
            <input type="checkbox" name="souvenir" value="1"/> Se souvenir de moi</label>
    </div>    
    <button type="submit" class="btn btn_primary col-sm-12">Se connecter</button>
</form>
</br>
<a href="index.php"><button title="Retourner à la page d'accueil." class="btn btn_primary  col-sm-12">Retour</button></a>
</br>
<p>Pas encore de compte, créez en un ! <a href="inscription.php">Inscription</a>

    <?php
    require'include/footer.php';
    