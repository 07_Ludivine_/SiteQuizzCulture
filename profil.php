<?php
require_once 'include/config.php';
require_once 'include/functions.php';

logged_only();

require_once 'include/header.php';
require_once 'include/nav_G.php';

?>
<?php if($_SESSION['auth']->acces_idacces==2):?>
   <a href="creation_quiz.php"><button title="Créer un quiz" class="btn btn_primary  col-sm-12">Créer un quiz</button></a>
   <a href="verifier_quiz.php"><button title="Vérifier, signaler les quiz qui n'ont pas été validé" class="btn btn_primary  col-sm-12">Valider des quiz</button></a>
<?php elseif($_SESSION['auth']->acces_idacces>2):?>
   <a href="creation_quiz.php"><button title="Créer un quiz" class="btn btn_primary  col-sm-12">Créer un quiz</button></a>
   <a href="verifier_quiz.php"><button title="Vérifier, signaler les quiz qui n'ont pas été validé" class="btn btn_primary  col-sm-12">Valider des quiz</button></a>
   <a href="supprimer_quiz.php"><button title="Aller à la page de suppression validation de quiz" class="btn btn_primary  col-sm-12">Supprimer ou valider des quiz</button></a>
<?php else:?>
   <a href="demande.php"><button title="Faire évoluer vos droit pour accéder à de nouvelles fonctionnalités" class="btn btn_primary  col-sm-12">Demander un compte de Contributeur</button></a>
<?php endif;?>
<h1>Bonjour <?= $_SESSION['auth']->pseudo; ?></h1>
<h2>Votre Profil <?= $_SESSION['auth']->niveauAcces;?>:</h2>

<table>
    <tr>
        <td>Pseudo</td>
        <td><?= $_SESSION['auth']->pseudo;?></td>
    </tr>
    <tr>
        <td>Prénom</td>
        <td><?= $_SESSION['auth']->prenom;?></td>
    </tr>
    <tr>
        <td>Nom</td>
        <td><?= $_SESSION['auth']->nom;?></td>
    </tr>
    <tr>
        <td>Email</td>
        <td><?= $_SESSION['auth']->email;?></td>
    </tr>
</table>

<a href="majProfil.php"><button class="btn btn_primary col-sm-7">Modifier mes informations</button></a></br>
<a href="changeMdp.php"><button class="btn btn_primary col-sm-7">Changer de mot de passe</button></a>
</br>
<a href="deconnexion.php"><button title="Quitter mon espace perso" class="btn btn_primary  col-sm-7">Se déconnecter</button></a>
</br>
<?php
require 'include/footer.php'; 