<?php

$user_id = $_GET['id'];
$token = $_GET['token'];
require 'include/config.php';
$req = $pdo->prepare('SELECT * FROM membre WHERE idmembre= ?');
$req->execute([$user_id]);
$user = $req->fetch();
session_start();
 
if($user && $user->confirmation_token == $token){
  
   $pdo->prepare('UPDATE membre SET confirmation_token = NULL, confirmed_at = NOW() WHERE idmembre=?')->execute([$user_id]);
   $_SESSION['flash']['success'] = "Votre inscription a bien été validée";
   $_SESSION['auth']= $user;
   header('Location: profil.php');

} else {
    $_SESSION['fash']['danger'] = 'Votre lien de confirmation n\'est plus valide';
    header('Location: connexion.php');    
}
