<?php

require 'Slim/Slim.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();



// Login Function which will Receive Http Post Request

$app->post('/login', function() {

    $app = \Slim\Slim::getInstance();

    $email = $app->request()->post('email');
    $mdp = $app->request()->post('mdp');


    try {
        $db = getDB();

        $sth = $db->prepare("select count(*) as count from membre WHERE email=:email AND mdp=:mdp");


        $sth->bindParam(':mdp', $mdp, PDO::PARAM_INT);
        $sth->bindParam(':email', $email, PDO::PARAM_INT);

        $sth->execute();

        $row = $sth->fetch();
        var_dump($email);
        var_dump($mdp);
        var_dump($row['count']);
        $app->response->setStatus(200);
        $app->response()->headers->set('Content-Type', 'application/json');

        if ($row['count'] > 0) {

            $output = array(
                'status' => "1",
                'login' => "sucess",
            );
        } else {

            $output = array(
                'status' => "0",
                'login' => "fail",
            );
        }
    } catch (Exception $ex) {

        $output = array(
            'status' => "2",
            'login' => "error",
        );
    }

    // $object = (object) $output;

    echo json_encode($output);
    $db = null;
});


// SignUP Function which will Receive Http Post Request
$app->post('/signup', function() {

    $app = \Slim\Slim::getInstance();
    $pseudo = $app->request()->post('pseudo');
    $prenom = $app->request()->post('prenom');
    $nom = $app->request()->post('nom');
    $email = $app->request()->post('email');
    $mdp = $app->request()->post('mdp');
    var_dump($email);
    $app->response->setStatus(200);
    $app->response()->headers->set('Content-Type', 'application/json');

    try {
        $db = getDB();

        $sth = $db->prepare("select count(*) as count from membre WHERE email=:email");


        $sth->bindParam(':email', $email, PDO::PARAM_INT);

        $sth->execute();

        $row = $sth->fetch();



        if ($row['count'] > 0) {

            $output = array(
                'status' => "0",
                'operation' => "student already registered"
            );

            echo json_encode($output);
            $db = null;

            return;
        } else {

            $sth = $db->prepare("INSERT INTO membre (pseudo, prenom, nom, email, mdp) 
				 VALUES(:pseudo, :prenom, :nom, :email, :mdp)");
            $sth->bindParam(':pseudo', $name, PDO::PARAM_INT);
            $sth->bindParam(':prenom', $email, PDO::PARAM_INT);
            $sth->bindParam(':nom', $name, PDO::PARAM_INT);
            $sth->bindParam(':email', $email, PDO::PARAM_INT);
            $sth->bindParam(':mdp', $pass, PDO::PARAM_INT);


            $sth->execute();



            $output = array(
                'status' => "1",
                'operation' => "success"
            );

            echo json_encode($output);
            $db = null;

            return;
        }
    } catch (Exception $ex) {

        echo $ex;
    }
});

function getDB() {
    $dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "";
    $dbname = "sitequizz";

    $mysql_conn_string = "mysql:host=$dbhost;dbname=$dbname";
    $dbConnection = new PDO($mysql_conn_string, $dbuser, $dbpass);
    $dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $dbConnection;
}

$app->run();
?>