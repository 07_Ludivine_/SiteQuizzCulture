<?php

function debug($variable) {
    echo'<pre>' . print_r($variable, true) . '</pre>';
}

function str_random($length) {
    $alphabet = "0123456789AZERTYUIOPQSDFGHJKLMWXCVBNazertyuiopqsdfghjklmwxcvbn";
    return substr(str_shuffle(str_repeat($alphabet, $length)), 0, $length);
}

//fonction de création du token aléatoire pour la validation des comptes

function logged_only() {
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }
    if (!isset($_SESSION['auth'])) {
        $_SESSION['flash']['danger'] = "Vous n'avez pas le droit d'accéder à cette page";
        header('Location: connexion.php');
        exit();
    }
}

/* fonction pour vérifier si l'utilisateur est connecté et qui redirige vers la 
  page de connexion avec un message d'information en cas d'absence de session ouverte */

function admin_only() {
    if ($_SESSION['auth']->acces_idacces != 3) {
        $_SESSION['flash']['danger'] = "Vous n'avez pas le droit d'accéder à cette page";
        header('Location: profil.php');
        exit();
    }
}
/*fonction pour vérifier le niveau administrateur et qui redirige si ce n'est pas le cas*/
function contrib_only() {
    if ($_SESSION['auth']->acces_idacces < 2) {
        $_SESSION['flash']['danger'] = "Vous n'avez pas le droit d'accéder à cette page";
        header('Location: profil.php');
        exit();
    }
}
/*fonction pour vérifier le niveau contributeur et qui redirige si ce n'est pas le cas*/