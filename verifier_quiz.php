<?php
require'include/header.php';
require'include/nav_G.php';
require'include/alert.php';
logged_only();//Fonction qui verifie que l'utilisateur et connecté, sinon redirection vers connexion
contrib_only();//Fonction qui verifie que les droit d'accès sont d'un niveau suffisant, sinon retour vers profil

if (isset($_POST["valider"])) {
    $idquestion = htmlspecialchars($_POST['idquestion']);
    $reqvalid = $pdo->prepare('UPDATE question SET verifie=1 WHERE idquestion=?');
    $reqvalid->execute([$idquestion]);
}//code qui valide la question, en changeant la valeur de verifie à 1 dans la BDD, si le bouton valider est cliqué par le contributeur
if (isset($_POST["signaler"])) {
    $idquestion = htmlspecialchars($_POST['idquestion']);
    $reqsignal = $pdo->prepare('UPDATE question SET verifie=99 WHERE idquestion=?');
    $reqsignal->execute([$idquestion]);
}//code qui signale la question, en changeant la valeur de verifie à 99 dans la BDD, si le bouton signaler est cliqué par le contributeur
?>
<?php
$reqn = $pdo->prepare('SELECT * FROM question WHERE verifie=0');
$reqn->execute();
//requete SQL qui selectionne dans la table question celles qui n'ont pas été vérifiés
while ($data = $reqn->fetch()) {//boucle qui affiche les informations des questions non vérifiées afin qu'elles puissent être vérifiées ou signalées par le contributeur
    ?>
    <label for="<?php $data->idquestion; ?>" class="col-sm-12">Question n°<?php echo $data->idquestion; ?>:</label>
    <div class="form-group col-sm-12">
        <p>Enoncé:<?php echo $data->enonce_question; ?></p>
        <p class="col-sm-6 btn_primary btn">Choix 1:<?php echo $data->choix_rep1; ?></p>
        <p class="col-sm-6 btn_primary btn">Choix 2:<?php echo $data->choix_rep2; ?></p>
        <p class="col-sm-6 btn_primary btn">Choix 3:<?php echo $data->choix_rep3; ?></p>
        <p class="col-sm-6 btn_primary btn">Choix 4:<?php echo $data->choix_rep4; ?></p>
        <p class="col-sm-6 btn_primary btn">Réponse:<?php echo $data->reponse; ?></p>
    </div>
    <form action="" method="post">
        <input type="hidden" value="<?php echo $data->idquestion; ?>" name="idquestion"/>
        <button class="col-sm-12" type="submit" name="valider">Valider</button>
    </form>
    <form action="" method="post">  
        <input type="hidden" value="<?php echo $data->idquestion; ?>" name="idquestion"/>
        <button class="col-sm-12" type="submit" name="signaler">Signaler</button>
    </form>
<?php } ?>
</br>
<a href="profil.php"><button title="Retour à la page profil" class="btn btn_primary  col-sm-12">Retour</button></a>
</br>
<?php
require"include/footer.php";
