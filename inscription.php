<?php
require_once 'include/config.php';
require_once'include/functions.php';
require_once 'include/header.php';
require_once 'include/nav_G_I.php';
?>
<?php
if (!empty($_POST)) {

    $errors = array();

    if (empty($_POST['pseudo']) || !preg_match('/^[a-zA-Z0-9_]+$/', $_POST['pseudo'])) {
        $errors['pseudo'] = "Votre Pseudo n'est pas valide (aphanumérique)";
    } else {
        $reqpseudo = $pdo->prepare('SELECT idmembre FROM membre WHERE pseudo = ?');
        $reqpseudo->execute([$_POST['pseudo']]);
        $pseudo = $reqpseudo->fetch();
        if ($pseudo) {
            $errors['pseudo'] = "Ce pseudo est déjà pris";
        }
    }
    if (empty($_POST['prenom']) || !preg_match("#^([a-zàáâäçèéêëìíîïñòóôöùúûü]+(( |')[a-z]+)*)+([-]([a-z]+(( |')[a-z]+)*)+)*$#iu", $_POST['prenom'])) {
        $errors['prenom'] = "Votre Prénom n'est pas valide (uniquement alphabétique)";
    }
    if (empty($_POST['nom']) || !preg_match("#^([a-zàáâäçèéêëìíîïñòóôöùúûü]+(( |')[a-z]+)*)+([-]([a-z]+(( |')[a-z]+)*)+)*$#iu", $_POST['nom'])) {
        $errors['nom'] = "Votre Nom n'est pas valide (uniquement alphabétique)";
    }
    if (empty($_POST['email']) || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
        $errors['email'] = "Votre Email n'est pas valide";
    } else {
        $reqemail = $pdo->prepare('SELECT idmembre FROM membre WHERE email = ?');
        $reqemail->execute([$_POST['email']]);
        $email = $reqemail->fetch();
        if ($email) {
            $errors['email'] = "Cet Email est déjà associé à un compte utilisateur";
        }
    }
    if ($_POST['email2'] != $_POST['email']) {
        $errors['email2'] = "Vos Email ne sont pas identiques";
    }
    if (empty($_POST['mdp']) || empty($_POST['mdp2']) || $_POST['mdp2'] != $_POST['mdp']) {
        $errors['mdp'] = "Vos Mots de passes ne sont pas identiques";
    }
    if (empty($errors)AND ! empty($_POST['pseudo'])AND ! empty($_POST['prenom'])AND ! empty($_POST['nom'])AND ! empty($_POST['email'])AND ! empty($_POST['mdp'])) {
        $req = $pdo->prepare("INSERT INTO membre SET pseudo = ?, prenom = ?, nom = ?, email = ?, mdp = ?"); //, confirmation_token = ?
        $mdp = password_hash($_POST['mdp'], PASSWORD_BCRYPT);
//    $token = str_random(60);
        $req->execute([$_POST['pseudo'], $_POST['prenom'], $_POST['nom'], $_POST['email'], $mdp]); //, $token
        /* $user_id = $pdo->lastInsertId(); 

          mail($_POST['email'], 'Confirmation de votre compte', "Afin de valider votre compte merci de cliquer sur ce lien\n\nhttp://local.dev/Lab/Comptes/confirm.php?id=$user_id&token=$token");

          $_SESSION['flash']['success']= 'Afin de valider votre inscription, un email de confirmation vous a été envoyé'; */
        header('Location: connexion.php');
        exit();
    }
}
?>

<h1>s'inscrire</h1>

<?php if (!empty($errors)): ?>
    <div class="alert alert-danger">
        <p>Vous n'avez pas rempli le formulaire correctement</p>
        <ul>
    <?php foreach ($errors as $error): ?>
                <li><?= $error; ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>

<form action="" method="POST">

    <div class="form-group">
        <label for="pseudo" >Votre Pseudo</label>
        <input class="form-control" type="text" name="pseudo" id="pseudo" placeholder="Choisissez un Pseudo" value="<?php if (isset($_POST['pseudo'])) {
    echo $_POST['pseudo'];
} ?>" required/>
    </div>
    <div class="form-group">
        <label for="prenom" >Votre Prénom</label>
        <input class="form-control" type="text" name="prenom" id="prenom" placeholder="Indiquez votre Prénom" value="<?php if (isset($_POST['prenom'])) {
    echo $_POST['prenom'];
} ?>" required />
    </div>
    <div class="form-group">
        <label for="nom" >Votre Nom</label>
        <input class="form-control" type="text" name="nom" id="nom" placeholder="Indiquer votre Nom" value="<?php if (isset($_POST['nom'])) {
    echo $_POST['nom'];
} ?>"required/>
    </div>
    <div class="form-group">
        <label for="email" >Votre Email</label>
        <input class="form-control" type="email" name="email" id="email" placeholder="Indiquez votre Email"value="<?php if (isset($_POST['email'])) {
    echo $_POST['email'];
} ?>"required/>
    </div>
    <div class="form-group">
        <label for="email2" >Verification de votre Email</label>
        <input class="form-control" type="email" name="email2" id="email2" placeholder="Confirmez votre Email" required/>
    </div>
    <div class="form-group">
        <label for="mdp" >Votre Mot de passe</label>
        <input class="form-control" type="password" name="mdp" id="mdp" placeholder="Choisissez un Mot de passe" required/>
    </div>
    <div class="form-group">
        <label for="mdp2" >Confirmation Mot de passe</label>
        <input class="form-control" type="password" name="mdp2" id="mdp2" placeholder="Confirmez votre Mot de passe" required/>
    </div>
    <button type="submit" class="btn btn_primary col-sm-12">S'inscrire</button>

</form>
</br>
<a href="index.php"><button title="Retourner à la page d'accueil." class="btn btn_primary  col-sm-12">Retour</button></a>
</br>
<p>Vous avez déjà un compte, connectez-vous ! <a href="connexion.php">Connexion</a>

<?php
require 'include/footer.php';
?>