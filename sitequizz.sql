-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Jeu 25 Janvier 2018 à 14:40
-- Version du serveur :  5.7.14
-- Version de PHP :  7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `sitequizz`
--

-- --------------------------------------------------------

--
-- Structure de la table `acces`
--

CREATE TABLE `acces` (
  `idacces` int(11) NOT NULL,
  `niveauAcces` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `acces`
--

INSERT INTO `acces` (`idacces`, `niveauAcces`) VALUES
(1, 'utilisateur'),
(2, 'contributeur'),
(3, 'administrateur');

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

CREATE TABLE `categorie` (
  `idcategorie` int(11) NOT NULL,
  `niveau_idniveau` int(11) NOT NULL,
  `matiere_idmatiere` int(11) NOT NULL,
  `nom_categorie` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `categorie`
--

INSERT INTO `categorie` (`idcategorie`, `niveau_idniveau`, `matiere_idmatiere`, `nom_categorie`) VALUES
(101, 1, 1, NULL),
(102, 1, 2, NULL),
(103, 1, 3, NULL),
(201, 2, 1, NULL),
(202, 2, 2, NULL),
(203, 2, 3, NULL),
(301, 3, 1, NULL),
(302, 3, 2, NULL),
(303, 3, 3, NULL),
(401, 4, 1, NULL),
(402, 4, 2, NULL),
(403, 4, 3, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `matiere`
--

CREATE TABLE `matiere` (
  `idmatiere` int(11) NOT NULL,
  `titre_matiere` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `matiere`
--

INSERT INTO `matiere` (`idmatiere`, `titre_matiere`) VALUES
(1, 'Francais'),
(2, 'Mathematique'),
(3, 'Histoire Geographie');

-- --------------------------------------------------------

--
-- Structure de la table `membre`
--

CREATE TABLE `membre` (
  `idmembre` int(11) NOT NULL,
  `pseudo` varchar(255) DEFAULT NULL,
  `prenom` varchar(255) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `mdp` text,
  `confirmation_token` varchar(60) DEFAULT NULL,
  `confirmed_at` datetime DEFAULT NULL,
  `reset_token` varchar(60) DEFAULT NULL,
  `remember_token` varchar(250) DEFAULT NULL,
  `acces_idacces` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `membre`
--

INSERT INTO `membre` (`idmembre`, `pseudo`, `prenom`, `nom`, `email`, `mdp`, `confirmation_token`, `confirmed_at`, `reset_token`, `remember_token`, `acces_idacces`) VALUES
(1, 'user', 'Mathilde', 'Pauzin', 'test@hotmail.fr', '$2y$10$nkLWZSKRLEpw9SRTVfquyuwn6GomtjLrahXFe/cVP86r5SRWJf76S', NULL, NULL, NULL, NULL, 3);

-- --------------------------------------------------------

--
-- Structure de la table `niveau`
--

CREATE TABLE `niveau` (
  `idniveau` int(11) NOT NULL,
  `nom_niveau` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `niveau`
--

INSERT INTO `niveau` (`idniveau`, `nom_niveau`) VALUES
(1, 'Maternelle'),
(2, 'Primaire'),
(3, 'College'),
(4, 'Lycee');

-- --------------------------------------------------------

--
-- Structure de la table `question`
--

CREATE TABLE `question` (
  `idquestion` int(11) NOT NULL,
  `enonce_question` text,
  `choix_rep1` varchar(200) DEFAULT NULL,
  `choix_rep2` varchar(200) DEFAULT NULL,
  `choix_rep3` varchar(200) DEFAULT NULL,
  `choix_rep4` varchar(200) DEFAULT NULL,
  `reponse` varchar(200) DEFAULT NULL,
  `membre_idauteur` int(11) NOT NULL,
  `verifie` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `question`
--

INSERT INTO `question` (`idquestion`, `enonce_question`, `choix_rep1`, `choix_rep2`, `choix_rep3`, `choix_rep4`, `reponse`, `membre_idauteur`, `verifie`) VALUES
(1, 'ee', 'zz', 'dd', 'ff', 'qq', 'qq', 1, 0),
(2, 'yy', 'rr', 'ee', 'zz', 'rr', 'rr', 1, 0),
(3, '&lt;p&gt;gezeg&lt;p&gt;', 'rr', 'ee', 'zz', 'tt', 'tt', 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `question_categorie`
--

CREATE TABLE `question_categorie` (
  `question_idquestion` int(11) NOT NULL,
  `categorie_idcategorie` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `score`
--

CREATE TABLE `score` (
  `membre_idmembre` int(11) NOT NULL,
  `categorie_idcategorie` int(11) NOT NULL,
  `score` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `acces`
--
ALTER TABLE `acces`
  ADD PRIMARY KEY (`idacces`);

--
-- Index pour la table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`idcategorie`,`niveau_idniveau`,`matiere_idmatiere`),
  ADD KEY `fk_categorie_niveau1_idx` (`niveau_idniveau`),
  ADD KEY `fk_categorie_matiere1_idx` (`matiere_idmatiere`);

--
-- Index pour la table `matiere`
--
ALTER TABLE `matiere`
  ADD PRIMARY KEY (`idmatiere`);

--
-- Index pour la table `membre`
--
ALTER TABLE `membre`
  ADD PRIMARY KEY (`idmembre`,`acces_idacces`),
  ADD KEY `fk_membre_accès1_idx` (`acces_idacces`);

--
-- Index pour la table `niveau`
--
ALTER TABLE `niveau`
  ADD PRIMARY KEY (`idniveau`);

--
-- Index pour la table `question`
--
ALTER TABLE `question`
  ADD PRIMARY KEY (`idquestion`,`membre_idauteur`),
  ADD KEY `fk_question_membre1_idx` (`membre_idauteur`);

--
-- Index pour la table `question_categorie`
--
ALTER TABLE `question_categorie`
  ADD PRIMARY KEY (`question_idquestion`,`categorie_idcategorie`),
  ADD KEY `fk_question_has_categorie_categorie1_idx` (`categorie_idcategorie`),
  ADD KEY `fk_question_has_categorie_question1_idx` (`question_idquestion`);

--
-- Index pour la table `score`
--
ALTER TABLE `score`
  ADD PRIMARY KEY (`membre_idmembre`,`categorie_idcategorie`),
  ADD KEY `fk_membre_has_categorie_categorie1_idx` (`categorie_idcategorie`),
  ADD KEY `fk_membre_has_categorie_membre1_idx` (`membre_idmembre`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `categorie`
--
ALTER TABLE `categorie`
  MODIFY `idcategorie` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=404;
--
-- AUTO_INCREMENT pour la table `matiere`
--
ALTER TABLE `matiere`
  MODIFY `idmatiere` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `membre`
--
ALTER TABLE `membre`
  MODIFY `idmembre` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `niveau`
--
ALTER TABLE `niveau`
  MODIFY `idniveau` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `question`
--
ALTER TABLE `question`
  MODIFY `idquestion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `categorie`
--
ALTER TABLE `categorie`
  ADD CONSTRAINT `fk_categorie_matiere1` FOREIGN KEY (`matiere_idmatiere`) REFERENCES `matiere` (`idmatiere`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_categorie_niveau1` FOREIGN KEY (`niveau_idniveau`) REFERENCES `niveau` (`idniveau`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `membre`
--
ALTER TABLE `membre`
  ADD CONSTRAINT `fk_membre_accès1` FOREIGN KEY (`acces_idacces`) REFERENCES `acces` (`idacces`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `question`
--
ALTER TABLE `question`
  ADD CONSTRAINT `fk_question_membre1` FOREIGN KEY (`membre_idauteur`) REFERENCES `membre` (`idmembre`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `question_categorie`
--
ALTER TABLE `question_categorie`
  ADD CONSTRAINT `fk_question_has_categorie_categorie1` FOREIGN KEY (`categorie_idcategorie`) REFERENCES `categorie` (`idcategorie`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_question_has_categorie_question1` FOREIGN KEY (`question_idquestion`) REFERENCES `question` (`idquestion`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `score`
--
ALTER TABLE `score`
  ADD CONSTRAINT `fk_membre_has_categorie_categorie1` FOREIGN KEY (`categorie_idcategorie`) REFERENCES `categorie` (`idcategorie`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_membre_has_categorie_membre1` FOREIGN KEY (`membre_idmembre`) REFERENCES `membre` (`idmembre`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
