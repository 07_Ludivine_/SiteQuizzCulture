<?php
require 'include/config.php';
require'include/functions.php';
logged_only();

if (!empty($_POST)) {
    if (!empty($_POST['mdp']) && !empty($_POST['mdp2']) && $_POST['mdp2'] == $_POST['mdp']) {
        $password = password_hash($_POST['mdp'], PASSWORD_BCRYPT);
        $pdo->prepare('UPDATE membre SET mdp = ? WHERE idmembre=?')->execute([$password, $user->idmembre]); //ne mettre a jour que le mot de passe de l'utilisateur concerné
        $_SESSION['flash']['success'] = 'Votre mot de passe a bien été modifié';
        session_start();
        header('Location: profil.php');
        exit();
    } else {
        $errors['mdp'] = "Vos Mots de passes ne sont pas identiques";
    }
}
require 'include/header.php';
?>
<h1>Modifier Mot de passe</h1>
<form action="" method="POST">
    <div class="form-group">
        <label for="mdp" >Nouveau Mot de passe</label>
        <input class="form-control" type="password" name="mdp" id="mdp" placeholder="Choisissez un Mot de passe"/>
    </div>
    <div class="form-group">
        <label for="mdp2" >Confirmation Mot de passe</label>
        <input class="form-control" type="password" name="mdp2" id="mdp2" placeholder="Confirmez votre Mot de passe"/>
    </div>
    <button type="submit" class="btn col-sm-12">Valider</button>
</form>
</br>
<a href="profil.php"><button title="Retour au profil" class="btn btn_primary  col-sm-12">Retour</button></a>
</br>
<?php
require"include/footer.php";
