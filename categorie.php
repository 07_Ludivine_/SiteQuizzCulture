<?php
require'include/header.php';
require'include/nav_G.php';
require'include/alert.php';
?>
<h1>Catégorie:</h1>
<?php
$reqC = $pdo->prepare('SELECT * FROM categorie '
        . 'INNER JOIN matiere ON categorie.matiere_idmatiere=matiere.idmatiere '
        . 'INNER JOIN niveau ON categorie.niveau_idniveau=niveau.idniveau ');
$reqC->execute();
/* requete SQL pour recupérer les noms des matière et des niveaux associés à chaque catégorie */
/* boucle d'affichage de toutes les catégories, association niveau+matiere */
$niveau = 1;
while($niveau <= 7){
    $reqC->execute();
while ($categorie = $reqC->fetch()) {
    //while($i <= 7){
    if($categorie->niveau_idniveau == $niveau){
       
    ?>
    <li class="col-sm-3 btn_primary btn">
        <!--Création du lien vers les quizz des différentes catégorie -->
        <a href="quiz.php?idcategorie=<?php echo $categorie->idcategorie; ?>">
            <!--Affichage de la matière et du niveau correspondant à la catégorie -->
            <?php echo $categorie->titre_matiere . ' ' . $categorie->nom_niveau; ?>
        </a>
    </li>
<?php }}
?>
    <div class="col-lg-12"><p></p></div>
   
    <?php
$niveau++;} ?> 
</br>
<a href="index.php"><button title="Retour à l'accueil" class="btn btn_primary  col-sm-12">Retour</button></a>
<br/>
<?php
require 'include/footer.php';
