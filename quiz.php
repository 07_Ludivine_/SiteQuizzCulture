<?php
require_once 'include/config.php';
require_once'include/functions.php';
require_once 'include/header.php';
require_once 'include/nav_G.php';
logged_only();
?>
<?php
if (isset($_GET['idcategorie'])) {
    $reqcat = $pdo->prepare('SELECT * FROM categorie '
            . 'INNER JOIN question_categorie ON categorie.idcategorie = question_categorie.categorie_idcategorie '
            . 'INNER JOIN question ON question_categorie.question_idquestion = question.idquestion '
            . 'INNER JOIN matiere ON categorie.matiere_idmatiere=matiere.idmatiere '
            . 'INNER JOIN niveau ON categorie.niveau_idniveau=niveau.idniveau WHERE idcategorie =? ORDER BY RAND() LIMIT 6');
    $reqcat->execute([$_GET['idcategorie']]);
    $cat = $reqcat->fetch();

    if (!$cat) {
        $_SESSION['flash']['danger'] = "La page que vous cherchez n'existe pas!";
        header('Location: profil.php');
        exit();
    }
} else {
    header('Location: profil.php');
    exit();
}
$score = 0;
if (isset($_POST["choix"])) {
    $choix = htmlspecialchars($_POST['option']);
    if ($choix == $cat->reponse) {
        $score = $score + 1;
    } else {
        $score = $score;
    }
}
?>

<h1>Quiz</h1>

 <form action="" method="post">
<?php
while ($cat = $reqcat->fetch()) {
    ?>

        <div class="title">Question <?php echo $cat->titre_matiere . ' ' . $cat->nom_niveau; ?></div>
        <div id="question" class="question"><p>Enoncé: <?php echo $cat->enonce_question; ?></p></div>
        
            <input type="hidden" name="option" value="<?php echo $cat->choix_rep1; ?>"/>
            <button class="col-sm-6 btn_primary btn btn-question" name="choix">1: <?php echo $cat->choix_rep1; ?></button>

            <input type="hidden" name="option" value="<?php echo $cat->choix_rep2; ?>"/>
            <button class="col-sm-6 btn_primary btn btn-question" name="choix">2: <?php echo $cat->choix_rep2; ?></button>

            <input type="hidden" name="option" value="<?php echo $cat->choix_rep3; ?>"/>
            <button class="col-sm-6 btn_primary btn btn-question" name="choix">3: <?php echo $cat->choix_rep3; ?></button>

            <input type="hidden" name="option" value="<?php echo $cat->choix_rep4; ?>"/>
            <button class="col-sm-6 btn_primary btn btn-question" name="choix" >4: <?php echo $cat->choix_rep4; ?></button>

            </br>

    <?php
}
?>
     <button type="submit" class="btn btn_primary col-sm-12">Valider</button>
 </form>
</br>
<a href="categorie.php"><button title="Retour à la page catégorie" class="btn btn_primary  col-sm-12">Retour</button></a>
</br>
<script src="quiz-script.js"></script>
<?php
require 'include/footer.php';
