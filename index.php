<?php
require_once'include/header.php';
require_once'include/nav_G_I.php';
require_once'include/alert.php';
?>

<div class="btns-accueil">
    <div class="row">
        <div class="col-lg-6">
            <a href="inscription.php">
                <button class="btn btn-primary btn-accueil">Inscription</button>
            </a>
        </div>
        <div class="col-lg-6">
            <a href="connexion.php">
                <button class="btn btn-primary btn-accueil">Connexion</button>
            </a>
        </div>
    </div>
    <h1>Site de quizz</h1>
    <p>Bienvenue sur le site de quizz en ligne</p>
    <p>Une fois que vous aurez un compte utilisateur sur ce site, 
        vous pourrez tester votre culture dans différentes matières en 
        fonction de votre niveau</p>
    <p>Si vous le souhaitez et que vous en faite la demande, 
        vous pourrez aussi devenir contributeur. En créant des quiz 
        pour faire bénéficier les autre utilisateur de vos connaissances.</p>

    </br>
    <a href="profil.php">
        <button title="Retour au profil" class="btn btn_primary  col-sm-12">
            Retour
        </button>
    </a>
    </br>
    <?php
    require'include/footer.php';
    