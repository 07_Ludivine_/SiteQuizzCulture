<?php
require'include/header.php';
require'include/nav_G.php';
require'include/alert.php';
logged_only()
?>
<?php
if (!empty($_POST)) {

    $errors = array();


    $niveau = htmlspecialchars($_POST['niveau']);
    $matiere = htmlspecialchars($_POST['matiere']);
    $question = htmlspecialchars($_POST['enonce_question']);
    $choix_rep1 = htmlspecialchars($_POST['choix_rep1']);
    $choix_rep2 = htmlspecialchars($_POST['choix_rep2']);
    $choix_rep3 = htmlspecialchars($_POST['choix_rep3']);
    $choix_rep4 = htmlspecialchars($_POST['choix_rep4']);
    $reponse = htmlspecialchars($_POST['reponse']);
    $reqcat = $pdo->prepare('SELECT idcategorie FROM categorie WHERE niveau_idniveau = ? AND matiere_idmatiere=?');
    $reqcat->execute([$niveau, $matiere]);
    $idcategorie = $reqcat->fetch();
    if ($niveau != 0 AND $matiere != 0) {

        if ($idcategorie) {
            
        } else {
            $errors[categorie] = "Veuillez indiquer une matiere et un niveau pour votre question";
        }
    }
    if (empty($question)) {
        $errors['enonce_question'] = "Veuillez poser une question valide"; //mettre une indication pour l'erreur, changer les filtres
    } else {
        $reqquestion = $pdo->prepare('SELECT idquestion FROM question WHERE enonce_question = ?');
        $reqquestion->execute([$question]);
        $enonce_question = $reqquestion->fetch();
        if ($enonce_question) {
            $errors['enonce_question'] = "Cette question fait déjà partie de notre repertoire!";
        }
    }

    if (empty($choix_rep1) || empty($choix_rep2) || empty($choix_rep3) || empty($choix_rep4)) {
        $errors['choix_rep'] = "Veuillez indiquer 4 choix de réponse";
    }

    if (!preg_match('/^[a-zA-Z0-9_]+$/', $_POST['choix_rep1'] || !preg_match('/^[a-zA-Z0-9_]+$/', $_POST['choix_rep2']) || !preg_match('/^[a-zA-Z0-9_]+$/', $_POST['choix_rep3']) || !preg_match('/^[a-zA-Z0-9_]+$/', $_POST['choix_rep4']) || !preg_match('/^[a-zA-Z0-9_]+$/', $_POST['reponse']))) {
        $errors['caractere'] = "Vos choix de réponses contiennent un ou plusieurs caractère non valide";
    }

    if ($choix_rep1 != $reponse AND $choix_rep2 != $reponse AND $choix_rep3 != $reponse AND $choix_rep4 != $reponse) {
        $errors['reponse'] = "La réponse, doit faire partie des choix de réponse!";
    }

    if (empty($errors)AND $idcategorie AND ! empty($question)AND ! empty($_POST['choix_rep1'])AND ! empty($_POST['choix_rep2'])AND ! empty($_POST['choix_rep3'])AND ! empty($_POST['choix_rep4']) AND ! empty($_POST['reponse'])) {

        $req = $pdo->prepare("INSERT INTO question SET enonce_question = ?, choix_rep1= ?, choix_rep2 = ?, choix_rep3 = ?, choix_rep4 = ?, reponse = ?, membre_idauteur = ?, verifie = ?");
        $req->execute([$question, $choix_rep1, $choix_rep2, $choix_rep3, $choix_rep4, $reponse, $_SESSION['auth']->idmembre, 0]);

        $reqidquestion = $pdo->prepare('SELECT idquestion FROM question WHERE enonce_question = ?');
        $reqidquestion->execute([$question]);
        $idquestion = $reqidquestion->fetch();
        var_dump($idquestion);
        var_dump($idcategorie);
        if ($idquestion) {
            $req2 = $pdo->prepare("INSERT INTO question_categorie SET question_idquestion = ?, categorie_idcategorie= ?");
            $req2->execute([$idquestion->idquestion, $idcategorie->idcategorie]);
        }
        header('Location: creation_quiz.php');
        exit();
    }
}
?>

<h1>Création de quiz</h1>

<?php if (!empty($errors)): ?>
    <div class="alert alert-danger">
        <p>Vous n'avez pas créer votre question correctement</p>
        <ul>
    <?php foreach ($errors as $error): ?>
                <li><?= $error; ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>

<form action="" method="POST">
    <div class="form-group">
        <label for="niveau" id="classe" class="form-control">Niveau</label>
        <select name="niveau" class="form-control">
            <option value=0>Indiquez le niveau de la question</option>
<?php
$reqn = $pdo->prepare('SELECT * FROM niveau');
$reqn->execute();

while ($data = $reqn->fetch()) {
    ?>
                <option value="<?php echo $data->idniveau; ?>"><?php echo $data->nom_niveau; ?></option>
            <?php } ?>

        </select>   
    </div>
    <div class="form-group">
        <label for="matiere" id="classe" class="form-control">Matiere</label>
        <select name="matiere" class="form-control">
            <option value=0>indiquez la matiere de la question</option>
            <?php
            $reqm = $pdo->prepare('SELECT * FROM matiere');
            $reqm->execute();

            while ($data = $reqm->fetch()) {
                ?>
                <option value="<?php echo $data->idmatiere; ?>"><?php echo $data->titre_matiere; ?></option>
            <?php } ?>
        </select>   
    </div>
    <div class="form-group">
        <label for="enonce_question" >Enoncé de la question</label>
        <input class="form-control" type="text" name="enonce_question" id="enonce_question" placeholder="Poser votre question" value="<?php if (isset($_POST['enonce_reponse'])) {
                echo $_POST['enonce_reponse'];
            } ?>"required/>
    </div>
    <div class="form-group">
        <label for="choix_rep1" >1er choix de réponse:</label>
        <input class="form-control" type="text" name="choix_rep1" id="choix_rep1" placeholder="Indiquer un 1er choix de réponse à votre question" value="<?php if (isset($_POST['choix_rep1'])) {
                echo $_POST['choix_rep1'];
            } ?>"required />
    </div>
    <div class="form-group">
        <label for="choix_rep2" >2eme choix de réponse:</label>
        <input class="form-control" type="text" name="choix_rep2" id="choix_rep2" placeholder="Indiquer un 2eme choix de réponse à votre question" value="<?php if (isset($_POST['choix_rep2'])) {
                echo $_POST['choix_rep2'];
            } ?>" />
    </div>
    <div class="form-group">
        <label for="choix_rep1" >3eme choix de réponse:</label>
        <input class="form-control" type="text" name="choix_rep3" id="choix_rep3" placeholder="Indiquer un 3eme choix de réponse à votre question" value="<?php if (isset($_POST['choix_rep3'])) {
                echo $_POST['choix_rep3'];
            } ?>"required />
    </div>
    <div class="form-group">
        <label for="choix_rep4" >4eme choix de réponse:</label>
        <input class="form-control" type="text" name="choix_rep4" id="choix_rep4" placeholder="Indiquer un 1er choix de réponse à votre question" value="<?php if (isset($_POST['choix_rep4'])) {
                echo $_POST['choix_rep4'];
            } ?>"required />
    </div>
    <div class="form-group">
        <label for="reponse" >Réponse:</label>
        <input class="form-control" type="text" name="reponse" id="reponse" placeholder="Indiquer la réponse à votre question" value="<?php if (isset($_POST['reponse'])) {
                echo $_POST['reponse'];
            } ?>"required />
    </div>
    <button type="submit" class="btn btn_primary col-sm-12">Créer la question</button>

</form>
</br>
<a href="profil.php"><button title="Retourner à la page de profil." class="btn btn_primary  col-sm-12">Retour</button></a>
</br>

<?php
require 'include/footer.php';
